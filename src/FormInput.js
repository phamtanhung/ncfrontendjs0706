import React from 'react';

class NameForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = { value: '' };

    this.inputRef = React.createRef();
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentDidMount() {
    this.setState(
      { value: this.props.number }
    )
  }
  componentDidUpdate(prevProps, prevState) {
    const { number: prevNumber } = prevProps;
    const { number } = this.props;
    if (prevNumber !== number) {
      this.setState(
        { value: this.props.number }
      )
    }
  }
  handleChange(event) {
    var t0, t1;
    this.setState({ value: this.inputRef.current.value }, () => {
      console.log('callback after set state done', this.state.value);
      t1 = performance.now();  
      console.log("Call to doSomething took " + (t1 - t0) + " milliseconds.");
      if (this.state.value === '10') this.inputRef.current.style.border = '1px solid red';
    });
    t0 = performance.now();
    console.log('sync', this.state.value);
    
  }

  handleSubmit(event) {
    alert('A name was submitted: ' + this.state.value);
    event.preventDefault();
  }

  render() {
    console.log('method: render');

    return (
      <form onSubmit={this.handleSubmit}>
        <label>
          Name:
          <input ref={this.inputRef} type="text" value={this.state.value} onChange={this.handleChange} />
        </label>
        <input type="submit" value="Submit" />
      </form>
    );
  }
}

export default NameForm;