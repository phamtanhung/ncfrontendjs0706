import React, { Component } from 'react';
import { BrowserRouter as Router, Link, Switch, Route } from 'react-router-dom'

import './App.css';
import {
  About,
  Home,
  Recipes,
  RecipeDetail,
  Contact } from './views';
class App extends Component {
  render() {
    return (
      <main>
        <Router>

          <aside>
            <p>Navigation: </p>
            <Link to={`/home`}>Home</Link> |
              <Link to={`/about`}>About</Link> |
              <Link to={`/recipes`}>Recipes</Link> |
              <Link to={`/recipe-detail`}>RecipeDetail page</Link> |
              <Link to={`/contact-us`}>Contact Us</Link>
          </aside>
          <Switch>
            <Route path="/about" component={About} />
            <Route path="/home" component={Home} />
            <Route path="/recipes" component={Recipes} />
            <Route path="/recipe-detail" component={RecipeDetail} />
            <Route path="/contact-us" component={Contact} />
          </Switch>
        </Router>
      </main >
    );
  }
}


export default App;
