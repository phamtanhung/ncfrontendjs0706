const PI = 3.14;
const square = (number) => {
  return number * number;
}
export default PI;

export {
  square
};