import { About } from './About';
import { Home } from './Home';
import { Recipes } from './Recipes';
import { RecipeDetail } from './RecipeDetail';
import { Contact } from './Contact';

export {
  About,
  Home,
  Recipes,
  RecipeDetail,
  Contact
}