import React from 'react';

import { Badge } from '../Badge';

// const getTotal = (products) => {
//   var total=0;
//   products.map(item => {
//      total=total+item.salePrice;
//      return item;
//   });
//   return total;
// }

const getTotal2 = (products) => {
  return products.reduce((accumulator, currentValue) => {
    return accumulator + currentValue.salePrice;
  }, 0)
};


const Cart = (props) => {
  const total = getTotal2(props.cart);

  return (
    <div>
      <Badge count={props.cart.length} ></Badge>
      <p>
        <span>Total: {total}</span>
      </p>

    </div>
  )


}
export default Cart;
