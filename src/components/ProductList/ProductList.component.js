import React from 'react';
import { ProductItem } from '../ProductItem';


const ProductList = (props) => {
  return (
    <section className="recepies">
      <div className="container">
        <button onClick={() => { props.onSort('increase')}}>Sort A-Z</button>
        <button onClick={() => { props.onSort('descrease')}}>Sort Z-A</button>

        <ul className="recepies-list">
          {props.data.map(item => {
            return <ProductItem key={item.id} data={item} addProduct={props.addProduct} />
          })
          }
        </ul>
      </div>
    </section>
  )
}
export { ProductList };